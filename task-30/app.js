const app = {
    printHello: (name) => {
        return `hello, ${name}`;
    },

    exponentiate: (array) => {
        let result = array.map((x) => x ** 2);
        return result;
    },
};

module.exports = app;
