const fs = require('fs');

const data = 'some text';

fs.mkdir('./files', () => {
    console.log('folder created');
});

fs.writeFile('./files/test.txt', data, () => {
    console.log('data written');
});

fs.readFile('./files/test.txt', 'utf8', (error, data) => {
    console.log(`data from file: ${data}`);
});

setTimeout(() => {
    if (fs.existsSync('./files/test.txt')) {
        fs.unlink('./files/test.txt', () => {
            console.log('file deleted');
        });
    }
}, 2000);

setTimeout(() => {
    if (fs.existsSync('./files')) {
        fs.rmdir('./files', () => {
            console.log('folder deleted');
        });
    }
}, 4000);
