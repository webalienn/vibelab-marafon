function asyncOperation() {
    return new Promise((resolve) => {
        setTimeout(() => {
            resolve('end async operation');
        }, 5000);
    });
}

async function mainOperation() {
    console.log('start async operation');

    try {
        const result = await asyncOperation();
        console.log(result);
    } catch (error) {
        console.error('error: ', error);
    }
}

mainOperation();
