let str1 = 'abc';
let str2 = '4';
let num = 1;
let bool1 = true;
let bool2 = false;
let arr = [1, 4, 5, 6, 8];
let obj = {
    name: 'Alice',
    age: 10,
};

let str3 = str1 + num;
console.log(str3, typeof str2);

console.log(bool1 && bool2);
console.log(bool1 || bool2);

console.log(str2 == arr[1]);
console.log(str2 === arr[1]);

console.log(arr[0] + arr[2]);

console.log(obj.name, obj.age);
