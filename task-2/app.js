const { rejects } = require('node:assert');
const fs = require('node:fs');
const { resolve } = require('node:path');

console.log('start');

setTimeout(() => console.log('timeout-1'), 100);
setTimeout(() => console.log('timeout-2'), 0);

const myPromise = new Promise((resolve, reject) => {
    console.log('promise-1');
    resolve();
});

myPromise.then(() => console.log('resolve-promise'));

fs.writeFile('./test.txt', 'Hello world!', () => console.log('file written'));
fs.readFile('./test.txt', () => console.log('file read'));

process.nextTick(() => {
    console.log('nextTick-1');
    setTimeout(() => console.log('timeout-3'), 10);
});

console.log('end');
