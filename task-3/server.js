const http = require('http');

const server = http.createServer(async (request, response) => {
    await asyncOperation();

    response.writeHead(200, { 'Content-Type': 'text/plain' });
    response.write('Hello world!');
    response.end();
});

server.listen(8000, () => {
    console.log('Server running at http://localhost:8000/');
});

function asyncOperation() {
    return new Promise((resolve) => {
        setTimeout(resolve, 1000);
    });
}
