let a = 1;
let b = 0;
let userId = 5;
let array = [1, 3, 4, 5, 7, 8, 10, 6, 4];
let users = ['user1', 'user2', 'user3', 'user4'];

for (let i = 0; i < array.length; i++) {
    console.log(array[i]);
}

function getUserName(id) {
    if (id >= users.length) {
        throw 'user doesnt exist';
    } else {
        console.log(`user name: ${users[id]}`);
    }
}

try {
    user = getUserName(userId);
} catch (error) {
    console.log(error);
}

function divide(a, b) {
    if (b === 0) {
        throw 'cant divide by zero';
    } else {
        console.log(`result: ${a / b}`);
    }
}

try {
    c = divide(a, b);
} catch (error) {
    console.log(error);
}
