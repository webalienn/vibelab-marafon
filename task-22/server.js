const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
    res.send('route page');
});

app.get('/home', (req, res) => {
    res.send('home page');
});

app.use((req, res) => {
    res.status(404).send('page not found');
});

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
});
