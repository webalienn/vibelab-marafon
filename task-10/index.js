let a = 5;
let b = 6;

const sum1 = () => {
    return a + b;
};
console.log(sum1()); // 11

function sum2(a, b) {
    return a + b;
}
console.log(sum2(a, b)); // 11
console.log(sum2(1, b)); // 7

const multiply = function (a, b) {
    return a * b;
};
console.log(multiply(4, 4)); // 16

const square = (a) => {
    return a * a;
};
console.log(square(3)); // 9

const hello = () => console.log('Hello world!');
hello();
