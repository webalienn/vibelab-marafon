const express = require('express');
const app = express();
const port = 3000;

const { Pool } = require('pg');

const pool = new Pool({
    user: 'postgres',
    database: 'my_db',
    password: '1234',
    port: 5432,
    host: 'localhost',
});

app.use(express.json());

async function getDataFromDb(tableName) {
    try {
        const client = await pool.connect();

        try {
            const result = await client.query(`SELECT * FROM ${tableName}`);

            return result.rows;
        } finally {
            client.release();
        }
    } catch (err) {
        console.error('error', err);
    }
}

app.get('/data', async (req, res) => {
    try {
        const data = await getDataFromTable('users');

        res.json(data);
        console.log(data);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'server error' });
    }
});

// app.listen(port, () => {
//     console.log(`app listening on port ${port}`);
// });

module.exports = { getDataFromDb, pool };
