const mName = 'Bob';

function hello(name) {
    console.log(`Hello ${name}!`);
}

export { mName };
export default hello;
