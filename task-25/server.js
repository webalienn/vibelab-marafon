const express = require('express');
const app = express();
const assert = require('assert');
const port = 3000;

const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
const dbName = 'my_db';

async function main() {
    try {
        await client.connect();
        console.log('connected successfully');

        // db my_db exists with collection user
        const db = client.db(dbName);
        const collection = db.collection('user');
        const documents = await collection.find().toArray();
        console.log(documents);

        return 'done.';
    } catch (error) {
        console.error(error);
        return 'error occurred.';
    } finally {
        await client.close();
        console.log('connection closed');
    }
}

main();

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
});
