const express = require('express');
const app = express();
const port = 3000;

// логгирование запросов
app.use((req, res, next) => {
    console.log(`path: ${req.path}`);
    console.log(`method: ${req.method}`);
    next();
});

// аутентификация
app.use((req, res, next) => {
    if (!req.headers.authorization) {
        return res.status(401).json({ error: 'header not found' });
    }

    next();
});

// обработка всех ошибок
app.use((err, req, res, next) => {
    console.error(err);
    res.status(404).json({ error: 'error' });
});

app.get('/', (req, res) => {
    res.send('route page');
});

app.get('/home', (req, res) => {
    res.send('home page');
});

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
});
