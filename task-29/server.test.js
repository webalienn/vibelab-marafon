const { pool } = require('./server');
const { getDataFromDb } = require('./server');

describe('GET /data', () => {
    let mockRows;
    let mockResult;
    let mockQuery;
    let mockRelease;

    beforeEach(() => {
        jest.clearAllMocks();

        mockRows = [
            { name: 'Alice', age: 20 },
            { name: 'Bob', age: 30 },
        ];
        mockResult = { rows: mockRows };
        mockQuery = jest.fn().mockResolvedValue(mockResult);
        mockRelease = jest.fn();

        pool.connect = jest.fn().mockResolvedValue({
            query: mockQuery,
            release: mockRelease,
        });
    });

    test('should return data from db', async () => {
        const tableName = 'users';

        const result = await getDataFromDb(tableName);

        expect(pool.connect).toHaveBeenCalledTimes(1);
        expect(mockQuery).toHaveBeenCalledTimes(1);
        expect(mockQuery).toHaveBeenCalledWith(`SELECT * FROM ${tableName}`);
        expect(mockRelease).toHaveBeenCalledTimes(1);
        expect(result).toEqual(mockRows);
    });
});
