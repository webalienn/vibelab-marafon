const http = require('http');

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');

    res.end('Hello world!');

    console.log('request received');
});

server.listen(3000, 'localhost', () => {
    console.log('app listening on port 3000');
});
