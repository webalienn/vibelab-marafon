const app = require('../app');
const assert = require('chai').assert;
const expect = require('chai').expect;

describe('printHello function', () => {
    it('should return string', () => {
        const name = 'Bob';
        assert.typeOf(app.printHello(name), 'string');
    });

    it('should return "hello, Bob"', () => {
        const name = 'Bob';
        const res = 'hello, Bob';
        expect(app.printHello(name)).to.equal(res);
    });
});

describe('exponentiate function', () => {
    it('new array length should be equal prev array', () => {
        let array = [1, 2, 3];
        expect(app.exponentiate(array).length).to.equal(array.length);
    });
    it('should return new array', () => {
        let array = [1, 2, 3];
        let newArray = [1, 4, 9];
        expect(app.exponentiate(array)).to.deep.equal(newArray);
    });
});
