let users = [
    { name: 'Bob', tokens: 100 },
    { name: 'ALice', tokens: 900 },
    { name: 'David', tokens: 200 },
];

users.sort((a, b) => a.name.localeCompare(b.name));
console.log('sorted by name: ');
console.log(users);

users.sort((a, b) => a.tokens - b.tokens);
console.log('sorted by tokens: ');
console.log(users);

const filteredTokens = users.filter((user) => {
    return user.tokens < 500;
});

console.log('filtered tokens: ');
console.log(filteredTokens);

const findTokens = users.find((user) => {
    return user.tokens < 500;
});

console.log('find tokens: ');
console.log(findTokens);
