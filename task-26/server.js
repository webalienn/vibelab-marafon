const express = require('express');
const app = express();
const assert = require('assert');
const port = 3000;

const { MongoClient } = require('mongodb');
const url = 'mongodb://localhost:27017';
const client = new MongoClient(url);
const dbName = 'my_db';

app.use(express.json());

app.post('/add_user', async (req, res) => {
    try {
        await client.connect();
        console.log('connected successfully');

        const db = client.db(dbName);
        const collection = db.collection('user');
        const result = await collection.insertOne(req.body);
        console.log(result);

        res.status(200).send('user added successfully');
        console.error(error);
        res.status(500).send('error');
    } finally {
        await client.close();
        console.log('connection closed');
    }
});

app.get('/users', async (req, res) => {
    try {
        await client.connect();
        console.log('connected successfully');

        const db = client.db(dbName);
        const collection = db.collection('user');
        const documents = await collection.find().toArray();
        console.log(documents);

        res.status(200).json(documents);
    } catch (error) {
        console.error(error);
        res.status(500).send('error');
    } finally {
        await client.close();
        console.log('connection closed');
    }
});

app.put('/update_user/:id', async (req, res) => {
    try {
        await client.connect();
        console.log('connected successfully');

        const db = client.db(dbName);
        const collection = db.collection('user');
        const result = await collection.updateOne(
            { _id: req.params.id },
            { $set: req.body }
        );
        console.log(result);

        res.status(200).send('user updated successfully');
    } catch (error) {
        console.error(error);
        res.status(500).send('error');
    } finally {
        await client.close();
        console.log('connection closed');
    }
});

app.delete('/delete_user/:id', async (req, res) => {
    try {
        await client.connect();
        console.log('connected successfully');

        const db = client.db(dbName);
        const collection = db.collection('user');
        const result = await collection.deleteOne({
            _id: ObjectId(req.params.id),
        });
        console.log(result);

        res.status(200).send('user deleted successfully');
    } catch (error) {
        console.error(error);
        res.status(500).send('error');
    } finally {
        await client.close();
        console.log('connection closed');
    }
});

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
});
