const { Certificate } = require('crypto');
const express = require('express');
const app = express();
const path = require('path');
const port = 3000;

app.set('view engine', 'ejs');

const createPath = (page) => path.resolve(__dirname, 'views', `${page}.ejs`);

app.get('/', (req, res) => {
    const data = {
        message: 'route page',
    };
    res.render(createPath('index'), data);
});

app.get('/home', (req, res) => {
    const data = {
        message: 'home page',
    };
    res.render(createPath('home'), data);
});

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
});
