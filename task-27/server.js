const express = require('express');
const app = express();
const port = 3000;

const { Pool } = require('pg');

const pool = new Pool({
    user: 'postgres',
    database: 'password',
    password: '1234',
    port: 5432,
    host: 'localhost',
});

module.exports = { pool };
app.use(express.json());

app.get('/data', async (req, res) => {
    try {
        const { rows } = await pool.query('SELECT * FROM users');
        res.json(rows);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'server error' });
    }
});

app.post('/data', async (req, res) => {
    const { name, age } = req.body;
    try {
        await pool.query('INSERT INTO users (name, age) VALUES ($1, $2)', [
            name,
            age,
        ]);
        res.sendStatus(201);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'server error' });
    }
});

app.put('/data/:id', async (req, res) => {
    const id = req.params.id;
    const { name, age } = req.body;
    try {
        await pool.query('UPDATE users SET name = $1, age = $2 WHERE id = $3', [
            name,
            age,
            id,
        ]);
        res.sendStatus(200);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'server error' });
    }
});

app.delete('/data/:id', async (req, res) => {
    const id = req.params.id;
    try {
        await pool.query('DELETE FROM users WHERE id = $1', [id]);
        res.sendStatus(200);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: 'server error' });
    }
});

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
});
